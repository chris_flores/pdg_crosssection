#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>

// FUNCTIONS DEFINED BELOW__________________________________________________
Double_t Difference(Double_t x*, Double_t *par);
Double_t ConvertPlabToCenterMass(Double_t pLab);
Double_t ConvertRootSToPlab(Double_t rootS);
Int_t FindPointInGraph(TGraphErrors *gr, Double_t x);
TGraphAsymmErrors *ReadPDGDataFile(TString dataFile);


// GLOBALS__________________________________________________________________
TF1 *totalFit;
TF1 *elasticFit;

// MAIN ____________________________________________________________________
void plotCrossSection(){

  const int nEnergies(8);
  Double_t energy[nEnergies] = {7.7,11.5,14.5,19.6,27,39,62.4,200};
  
  TString totalCrossSectionFile = "pdg_data/rpp2014-pp_total.dat";
  TString elasticCrossSectionFile = "pdg_data/rpp2014-pp_elastic.dat";

  TGraphAsymmErrors *totalCrossSectionGraph = ReadPDGDataFile(totalCrossSectionFile);
  totalCrossSectionGraph->SetMarkerStyle(7);
  totalCrossSectionGraph->SetMarkerColor(kBlack);

  TGraphAsymmErrors *elasticCrossSectionGraph = ReadPDGDataFile(elasticCrossSectionFile);
  elasticCrossSectionGraph->SetMarkerStyle(7);
  elasticCrossSectionGraph->SetMarkerColor(kBlack);

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  canvas->SetLogy();
  canvas->SetLogx();
  canvas->SetTicks();

  canvas->SetRightMargin(.05);
  canvas->SetTopMargin(.05);
  canvas->SetBottomMargin(.12);

  Double_t xLow(1e-01), xHigh(1e+10), yLow(1.5), yHigh(500);
  TH1F *frame = canvas->DrawFrame(xLow,yLow,xHigh,yHigh);
  frame->GetYaxis()->SetTitle("p+p Cross Section (mb)");
  frame->GetXaxis()->SetTitle("P_{Lab} (GeV/c)");
  frame->GetXaxis()->SetTitleOffset(1.25);
  frame->SetTitleFont(62,"X");
  frame->SetTitleSize(.04,"X");
  frame->SetTitleFont(62,"Y");
  frame->SetTitleSize(.04,"Y");

  //Create Title
  TPaveText *title = new TPaveText(.33,.80,.92,.90,"NDC");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextAlign(32);
  title->SetTextFont(62);
  title->SetTextSize(.045);
  title->AddText("Inelastic p+p Cross Sections");
  title->AddText("for RHIC BES Energies");
  title->Draw("SAME");

  //Data Lables
  TPaveText *totalLabel = new TPaveText(.52,.68,.60,.72,"brNDC");
  totalLabel->SetFillColor(kWhite);
  totalLabel->SetBorderSize(0);
  totalLabel->SetTextFont(72);
  totalLabel->SetTextSize(.035);
  totalLabel->AddText("Total");
  totalLabel->Draw("SAME");

  TPaveText *elasticLabel = new TPaveText(.456,.302,.515,.342,"brNDC");
  elasticLabel->SetFillColor(kWhite);
  elasticLabel->SetBorderSize(0);
  elasticLabel->SetTextFont(72);
  elasticLabel->SetTextSize(.035);
  elasticLabel->AddText("Elastic");
  elasticLabel->Draw("SAME");

  TPaveText *inelasticLabel = new TPaveText(.48,.51,.56,.55,"brNDC");
  inelasticLabel->SetFillColor(kWhite);
  inelasticLabel->SetBorderSize(0);
  inelasticLabel->SetTextFont(72);
  inelasticLabel->SetTextSize(.035);
  inelasticLabel->AddText("Inelastic");
  inelasticLabel->Draw("SAME");
  
  //Create BES Lines
  TLine *besLowLine = new TLine(ConvertRootSToPlab(7.7),yLow,ConvertRootSToPlab(7.7),100);
  besLowLine->SetLineColor(kBlack);
  besLowLine->SetLineWidth(2);
  besLowLine->Draw("SAME");

  TLine *besHighLine = new TLine(ConvertRootSToPlab(62.4),yLow,ConvertRootSToPlab(62.4),100);
  besHighLine->SetLineColor(kBlack);
  besHighLine->SetLineWidth(2);
  besHighLine->Draw("SAME");

  TArrow *besLowArrow = new TArrow(ConvertRootSToPlab(8.2),95,ConvertRootSToPlab(12),95,.015,"<|");
  TArrow *besHighArrow = new TArrow(ConvertRootSToPlab(8.2),95,ConvertRootSToPlab(60),95,.015,"|>");
  besLowArrow->Draw();
  besHighArrow->Draw();
  
  TPaveText *besLabel = new TPaveText(.295,.618,.426,.688,"brNDC");
  besLabel->SetFillColor(kWhite);
  besLabel->SetBorderSize(0);
  besLabel->SetTextFont(62);
  besLabel->SetTextSize(.03);
  besLabel->AddText("RHIC BES");
  besLabel->AddText("Program");  
  besLabel->Draw("SAME");

  
  totalCrossSectionGraph->Draw("PZ");
  elasticCrossSectionGraph->Draw("PZ");

  ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(100000);
  //ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit","Migrad");
  //ROOT::Math::MinimizerOptions::SetMinimizerAlgorithm("MIGRAD");
  Double_t fitRangeMin(ConvertRootSToPlab(5.0));
  Double_t fitRangeMax(4.0E+09);
  const Int_t npx = 100000;

  //TOTAL CROSS SECTION FIT -- PDG 2015
  /*
  totalFit = new TF1("pdgFit_2015","[0]+((3.14*.389)/pow([1],2))*(pow(log10(pow(x,2)/pow(2*.938+[1],2)),2))+[2]*pow(pow(2*.938+[1],2)/pow(x,2),[3])+[4]*pow(pow(2*.938+[1],2)/pow(x,2),[5])",fitRangeMin,fitRangeMax);
  totalFit->SetParNames("P^{ab}","M","R_{1}^{ab}","#eta_{1}","R_{2}^{ab}","#eta_{2}");
  */
  
  totalFit = new TF1("pdgFit_2015","[0]+((3.14*.389)/pow([1],2))*(pow(log10(pow(x,2)/pow(2*.938+[1],2)),2))+[2]*pow(pow(2*.938+[1],2)/pow(x,2),[3])",fitRangeMin,fitRangeMax);
  totalFit->SetParNames("P^{ab}","M","R_{1}^{ab}","#eta_{1}");
  
  //Parameters
  Double_t Pab(0),  PabErr(0);
  Double_t M(0),    MErr(0);
  Double_t R1(0),   R1Err(0);
  Double_t Eta1(0), Eta1Err(0);
  Double_t R2(0),   R2Err(0);
  Double_t Eta2(0), Eta2Err(0);

  //Parameter Defs --- PDG 2015 for roots >= 5 GeV
  Pab  = 34.49; PabErr  = 0.21;
  M    = 2.127; MErr    = 0.015;
  R1   = 12.98; R1Err   = 0.26;
  Eta1 = 0.451; Eta1Err = 0.013;
  R2   = 7.38;  R2Err   = 0.11;
  Eta2 = 0.549; Eta2Err = 0.0070;

  //Set the Parameters
  totalFit->SetParameters(Pab,M,R1,Eta1,R2,Eta2);
  
  /*
  //Set the Parameter Limits
  Double_t errScale(2.0);
  totalFit->SetParLimits(0,  Pab-3.0*PabErr,    Pab+3.0*PabErr);
  totalFit->SetParLimits(1,  M-10*MErr,        M+10*MErr);
  
  totalFit->SetParLimits(2,  R1-errScale*R1Err,      R1+errScale*R1Err);
  totalFit->SetParLimits(3,  Eta1-10*Eta1Err,  Eta1+10*Eta1Err);
  totalFit->SetParLimits(4,  R2-errScale*R2Err,      R2+errScale*R2Err);
  totalFit->SetParLimits(5,  Eta2-10*Eta2Err,        Eta2+10*Eta2Err);
  */
  
  //totalFit->SetParameters(33.73,2.076,13.67,0.412,7.77,0.5626);
  //totalFit->SetParameters(34.71,2.15,12.72,0.462,7.35,.550);
  /*
  totalFit->SetParLimits(0,totalFit->GetParameter(0)-5*0.33,totalFit->GetParameter(0)+5*0.33);
  totalFit->SetParLimits(1,totalFit->GetParameter(1)-5*.016,totalFit->GetParameter(1)+5*0.016);
  totalFit->SetParLimits(2,totalFit->GetParameter(2)-5*.33,totalFit->GetParameter(2)+5*.33);
  totalFit->SetParLimits(3,totalFit->GetParameter(3)-5*0.017,totalFit->GetParameter(3)+5*0.017);
  totalFit->SetParLimits(4,totalFit->GetParameter(4)-5*0.18,totalFit->GetParameter(4)+5*0.18);
  totalFit->SetParLimits(5,totalFit->GetParameter(5)-2*0.0092,totalFit->GetParameter(5)+2*0.0092);
  */
  
  totalFit->SetLineColor(kBlue);
  totalFit->SetLineWidth(2);
  totalFit->SetNpx(npx);
  totalCrossSectionGraph->Fit(totalFit,"RM");
  totalFit->Draw("SAME");

  
  //TOTAL CROSS SECTION CONFIDENCE INTERVAL  
  TGraphErrors *totalConfidence = new TGraphErrors(npx);
  for (Int_t i=0; i<npx; i++){
    totalConfidence->SetPoint(i,fitRangeMin +  (pow(i,2)*(fitRangeMax-fitRangeMin) ) / pow(npx,2),0);
  }
  (TVirtualFitter::GetFitter())->GetConfidenceIntervals(totalConfidence,.683);
  totalConfidence->SetFillColor(kBlue-8);
  totalConfidence->SetFillStyle(3001);
  //totalConfidence->Draw("3");
  totalFit->Draw("SAME");
  

  //ELASTIC CROSS SECTION FIT
  elasticFit = new TF1("elasticCrossSectionFit","[0]+[1]/(pow(x,2)^[2])+[3]*log10(pow(x,2))",fitRangeMin,fitRangeMax);
  //elasticFit = new TF1("elasticCrossSectionFit","[0]+((3.14*.389)/pow([1],2))*(pow(log10(pow(x,2)/pow(2*.938+[1],2)),2))+[2]*pow(pow(2*.938+[1],2)/pow(x,2),[3])",fitRangeMin,fitRangeMax);
  elasticFit->SetParameters(62,10,.5,19);
  elasticFit->SetLineColor(kGreen+2);
  elasticFit->SetLineWidth(2);
  elasticFit->SetNpx(npx);
  elasticCrossSectionGraph->Fit(elasticFit,"RM");

  //INELASTIC CROSS SECTION
  TF1 *inelasticCurve = new TF1("inelasticCurve",Difference,fitRangeMin,fitRangeMax,0);
  inelasticCurve->SetLineColor(kRed);
  inelasticCurve->Draw("SAME");
  
  TGraphErrors *inelasticCrossSectionGraph = new TGraphErrors();
  for (Int_t i=0; i<elasticCrossSectionGraph->GetN(); i++){

    if (elasticCrossSectionGraph->GetX()[i] < fitRangeMin)
      continue;
    
    Double_t pLab    = elasticCrossSectionGraph->GetX()[i];
    Double_t eCS     = elasticCrossSectionGraph->GetY()[i];
    Double_t totalCS = totalFit->Eval(pLab);

    Double_t eCSErr = TMath::Max(elasticCrossSectionGraph->GetEYlow()[i],
				 elasticCrossSectionGraph->GetEYhigh()[i]);

    Int_t point = FindPointInGraph(totalConfidence,pLab);
    Double_t totalCSErr = totalConfidence->GetEY()[point];
    
    inelasticCrossSectionGraph->SetPoint(inelasticCrossSectionGraph->GetN(),
				    pLab,totalCS-eCS);
    inelasticCrossSectionGraph->SetPointError(inelasticCrossSectionGraph->GetN()-1,
					 0,sqrt(pow(totalCSErr,2) + pow(eCSErr,2)));
    
  }

  inelasticCrossSectionGraph->SetMarkerStyle(7);
  inelasticCrossSectionGraph->SetMarkerColor(kBlack);
  inelasticCrossSectionGraph->Draw("PZ");

  for (int iEnergy=0; iEnergy < nEnergies; iEnergy++){

    cout <<"RootS: " <<energy[iEnergy] <<"\t"
	 <<" InelCS: " <<inelasticCurve->Eval(ConvertRootSToPlab(energy[iEnergy])) <<endl;
    
  }
  cout <<"RootS: " <<2760 <<"\t"
       <<" InelCS: " <<inelasticCurve->Eval(ConvertRootSToPlab(2760)) <<endl;
  cout <<"RootS: " <<5020 <<"\t"
       <<" InelCS: " <<inelasticCurve->Eval(ConvertRootSToPlab(5020)) <<endl;
    
  //CREATE A TABLE OF THE RESULTS
  Double_t top(.49), rowSize(.032);
  Double_t left(.72), colSize(.1);
  Double_t colSpace(.01), rowSpace(.005);

  //Column 1
  Double_t row(1), col(1);
  Double_t colStart = col*left;
  Double_t rowStart = row*top;
  TPaveText *sqrtS = new TPaveText(colStart,rowStart-.05,colStart+colSize,rowStart,"NDC");
  sqrtS->SetFillColor(kWhite);
  sqrtS->SetBorderSize(0);
  sqrtS->SetTextAlign(22);
  sqrtS->SetTextFont(62);
  sqrtS->SetTextSize(.032);
  sqrtS->AddText("#sqrt{s_{NN}} (GeV)");
  sqrtS->Draw("SAME");

  TPaveText *col1Ene[nEnergies];
  rowStart = rowStart-.02;
  for (Int_t i=0; i<8; i++){
    rowStart = rowStart-rowSize-rowSpace;
    col1Ene[i] = new TPaveText(colStart,rowStart-rowSize,colStart+colSize,rowStart,"NDC");
    col1Ene[i]->SetFillColor(kWhite);
    col1Ene[i]->SetBorderSize(0);
    col1Ene[i]->SetTextAlign(22);
    col1Ene[i]->SetTextFont(60);
    col1Ene[i]->SetTextSize(.03);
    col1Ene[i]->AddText(Form("%g",energy[i]));
    col1Ene[i]->Draw("SAME");
  }

  row=1; col=2;
  colStart = colStart + colSize + colSpace;
  rowStart = row*top;
  TPaveText *sigma = new TPaveText(colStart,rowStart-.05,colStart+colSize,rowStart,"NDC");
  sigma->SetFillColor(kWhite);
  sigma->SetBorderSize(0);
  sigma->SetTextAlign(22);
  sigma->SetTextFont(62);
  sigma->SetTextSize(.032);
  sigma->AddText("#sigma_{Inel} (mb)");
  sigma->Draw("SAME");

  TPaveText *col2Sigma[nEnergies];
  rowStart = rowStart-.02;

  //return;
  
  Double_t totalCSError(0), elasticCSError(0), inelasticCSError(0);
  for (Int_t i=0; i<8; i++){
    rowStart = rowStart-rowSize-rowSpace;
    col2Sigma[i] = new TPaveText(colStart,rowStart-rowSize,colStart+colSize,rowStart,"NDC");
    col2Sigma[i]->SetFillColor(kWhite);
    col2Sigma[i]->SetBorderSize(0);
    col2Sigma[i]->SetTextAlign(22);
    col2Sigma[i]->SetTextFont(60);
    col2Sigma[i]->SetTextSize(.028);

    col2Sigma[i]->AddText(Form("%.1f",inelasticCurve->Eval(ConvertRootSToPlab(energy[i]))));

    col2Sigma[i]->Draw("SAME");
  }
  
  return;
  
  
  
}


//__________________________________________________________________________
TGraphAsymmErrors *ReadPDGDataFile(TString dataFile){

  TGraphAsymmErrors *gr = new TGraphAsymmErrors();
  
  const int nColumns(14);
  std::ifstream inFile(dataFile);

  char line[200];
  char *column[nColumns] = {};
    
  for (int i=0; i<11; i++){
    inFile.getline(line,200);
  }

  while(!inFile.eof()){

    inFile.getline(line,200);

    column[0] = strtok(line," ");

    for (int i=1; i<nColumns; i++){
      column[i] = strtok(0," ");
    }

    //DO NOT USE POINTS THAT HAVE ZERO ERROR
    if (TString::TString(column[5]).Atof() == 0 ||
	TString::TString(column[6]).Atof() == 0)
      continue;

    //X and Y
    Double_t pLab = TString::TString(column[1]).Atof();
    Double_t crossSection = TString::TString(column[4]).Atof();

    //X Err
    Double_t xErrLow  = fabs(TString::TString(column[2]).Atof()-pLab);
    Double_t xErrHigh = fabs(TString::TString(column[3]).Atof()-pLab);

    //Y Statistical Error
    Double_t statErrLow  = TString::TString(column[5]).Atof();
    Double_t statErrHigh = TString::TString(column[6]).Atof();

    //Y Systematic Error - is reported as percent so we have to change it
    Double_t sysErrLow   = (TString::TString(column[7]).Atof() * crossSection) / 100.0;
    Double_t sysErrHigh  = (TString::TString(column[8]).Atof() * crossSection) / 100.0;

    //Y Total Error - Stat and Sys Error in quadrature
    Double_t yTotErrLow  = sqrt(pow(statErrLow,2) + pow(sysErrLow,2));
    Double_t yTotErrHigh = sqrt(pow(statErrHigh,2) + pow(sysErrHigh,2));
      
    //Set the X and Y Values
    gr->SetPoint(gr->GetN(),pLab,crossSection);

    //Set the Errors
    gr->SetPointError(gr->GetN()-1,
		      xErrLow,    xErrHigh,
		      yTotErrLow, yTotErrHigh);
    
  }//End Loop Over Lines of File

  return gr;
  
}


//__________________________________________________________________________
Double_t Difference(Double_t *x, Double_t *par){

  return totalFit->Eval(x[0]) - elasticFit->Eval(x[0]);
  
}

//__________________________________________________________________________
Double_t ConvertPlabToCenterMass(Double_t pLab){

  Double_t mass = 0.938;

  return sqrt(2.0*mass*(mass+sqrt(pow(mass,2)+pow(pLab,2))));
}

//___________________________________________________________________________
Double_t ConvertRootSToPlab(Double_t rootS){

  Double_t mass = 0.938;

  return sqrt(pow((pow(rootS,2)/(2.0*mass))-mass,2) - pow(mass,2));
  
}

//___________________________________________________________________________
Int_t FindPointInGraph(TGraphErrors *gr, Double_t x){

  //Search along the x axis of the graph until you find the point closest to x
  Double_t distance(999999999), bestDistance(99999999);
  Int_t bestPoint(-1);
  for (Int_t i=0; i<gr->GetN(); i++){
    distance = fabs(gr->GetX()[i] - x);

    if (distance < bestDistance){
      bestDistance = distance;
      bestPoint = i;
    }
  }
  return bestPoint;
}
